# Project 7 (P7) grading rubric

## Code reviews

- A TA / grader will be reviewing your code after the deadline.
- They will make deductions based on the Rubric provided below.
- To ensure that you don’t lose any points in code review, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General deductions:

- Did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. This deduction will become stricter for future projects. (-3)
- Used concepts/modules (ex: pandas) not covered in class yet (You may use built-in functions that you have been instructed to use) (-3)
- import statements are not mentioned in the required cell at the top of the notebook (-1)
- Hardcoded answers or data structures (full points)

### Question specific guidelines:

- `cell` (3)
	- Function does not typecast based on column names (-1)
	- Function does not multiply the values in the pop column by 1000 (-1)
	- Function is defined more than once (-1)

- Q1 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q2 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q3 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q4 (4)
	- Incorrect logic is used to answer (-1)
	- Recomputed variable defined in Q3 (-1)
	- Required functions are not used to answer (-1)

- Q5 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q6 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q7 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q8 (5)
	- Incorrect logic is used to answer (-2)
	- Hardcoded income levels (-1)
	- Required functions are not used to answer (-1)

- Q9 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- `get_col_dict` (3)
	- Incorrect logic is used in function (-2)
	- Function is defined more than once (-1)

- `dict_2015` (3)
	- Data structure is defined incorrectly (-3)

- `dict_2020` (3)
	- Data structure is defined incorrectly (-3)

- Q10 (2)
	- Required data structures are not used to answer (-1)

- Q11 (2)
	- Required data structures are not used to answer (-1)

- Q12 (3)
	- Incorrect logic is used to answer (-1)
	- Required data structures are not used to answer (-1)

- Q13 (4)
	- Incorrect logic is used to answer (-2)
	- Required data structures are not used to answer (-1)

- Q14 (4)
	- Incorrect logic is used to answer (-2)
	- Required data structures are not used to answer (-1)

- Q15 (4)
	- Incorrect logic is used to answer (-1)
	- Recomputed variables already defined in q13 and q14 (-1)
	- Required data structures are not used to answer (-1)

- `dict_2015` (2)
	- Incorrect rural_alb calculation in data structure (-2)

- `dict_2020` (2)
	- Incorrect rural_alb calculation in data structure (-2)

- Q16 (2)
	- Required data structures are not used to answer (-1)

- `rural_non_alb_bin_2015_dict` (5)
	- Data structure is defined incorrectly (-5)

- `rural_non_alb_bin_2020_dict` (5)
	- Data structure is defined incorrectly (-5)

- Q17 (2)
	- Required data structures are not used to answer (-1)

- Q18 (4)
	- Incorrect logic is used to answer (-2)
	- Required data structures are not used to answer (-1)

- Q19 (5)
	- Incorrect logic is used to answer (-2)
	- Required data structures are not used to answer (-2)

- Q20 (5)
	- Incorrect logic is used to answer (-2)
	- Required data structures are not used to answer (-2)
